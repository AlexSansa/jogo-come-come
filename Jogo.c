#include <stdio.h>
#include <stdlib.h>
#include <GL/freeglut.h>
#include <math.h>
#include <time.h>

GLdouble x, y, z, theta_yaw, theta_pitch, vx, vy, vz;

struct cubo {
	GLfloat x, y, z;
	GLfloat tamanho;
	int ponto;
};

struct cubo player1;
struct cubo player2;
struct cubo comida;


void colisao(){

	if(player1.x == comida.x && player1.z == comida.z){
        
        player1.ponto += 100;
	comida.x = rand()%20;
	comida.z = rand()%20;
	printf("\nPlayer 1 - %d  || Player 2 - %d\n",player1.ponto,player2.ponto);
        
        }
        
        if(player2.x == comida.x && player2.z == comida.z){

	player2.ponto += 100;
	comida.x = rand()%20;
	comida.z = rand()%20;
	printf("\nPlayer 1 - %d  || Player 2 - %d\n",player1.ponto,player2.ponto);
	
	}



}
void inicializar()
{
    GLfloat luzAmbiente[4]={0.1,0.1,0.1,0.1}; 
    GLfloat luzDifusa[4]={0.7,0.7,0.7,1.0};       // "cor" 
    GLfloat luzEspecular[4]={0.7, 0.7, 0.7, 1.0};// "brilho" 
    GLfloat posicaoLuz[4]={10, 80, 10, 1.0};

    // Capacidade de brilho do material
    GLfloat especularidade[4]={0,0,0,1.0}; 
    GLint especMaterial = 60;

     // Especifica que a cor de fundo da janela será preta
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // Habilita o modelo de colorização de Gouraud
    glShadeModel(GL_SMOOTH);

    // Define a refletância do material 
    glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
    // Define a concentração do brilho
    glMateriali(GL_FRONT,GL_SHININESS,especMaterial);

    // Ativa o uso da luz ambiente 
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

    // Define os parâmetros da luz de número 0
    glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente); 
    glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa );
    glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular );
    glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );

    // Habilita a definição da cor do material a partir da cor corrente
    glEnable(GL_COLOR_MATERIAL);
    //Habilita o uso de iluminação
    glEnable(GL_LIGHTING);
    // Habilita a luz de número 0
    glEnable(GL_LIGHT0);
    // Habilita o depth-buffering
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);

    x = -20;
    y = 30;
    z = 9.5;

    theta_yaw = 0;
    theta_pitch = 5;

    player1.x = 0.5;
    player1.y = 0.5;
    player1.z = 1.5;
    player1.tamanho = 1;

    player2.x = 0.5;
    player2.y = 0.5;
    player2.z = 19.5;
    player2.tamanho = 1;

    comida.x = rand()%20;
    comida.y = 0.5;
    comida.z = rand()%20;
    comida.tamanho = 1;

}

void myReshape(int w, int h) {
    glViewport (0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective (60, (float)(w)/h, 0.1, 100);
    glMatrixMode(GL_MODELVIEW);
    glutPostRedisplay();
}

void myDisplay(void) {
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vx = cos(theta_yaw);
    vz = sin(theta_yaw);
    vy = sin(theta_pitch);

    glLoadIdentity();
    gluLookAt(x, y, z,
              x + vx, y + vy, z + vz,
              0, 1,  0);

    for (float i = 0; i < 20; i++)
        for (float j = 0; j < 20; j++)
        {
            glPushMatrix();
                glTranslatef(i, 0, j);
		glScalef(20,0.5,20);
                glColor3f(0.5, 0.5, 0.5);
                glutSolidCube(1);
            glPopMatrix();
        }

    glPushMatrix();
        glTranslatef(player1.x, player1.y, player1.z);
        glColor3f(0, 1, 0);
        glutSolidCube(player1.tamanho);
        glColor3f(0,0,0);
        glutWireCube(player1.tamanho);
    glPopMatrix();

    glPushMatrix();
	glTranslatef(player2.x,player2.y,player2.z);
	glColor3f(0,0,1);
        glutSolidCube(player2.tamanho);
        glColor3f(0,0,0);
	glutWireCube(player2.tamanho);
    glPopMatrix();

    glPushMatrix();
	glTranslatef(comida.x,comida.y,comida.z);
	glColor3f(1,1,0);
        glutSolidCube(comida.tamanho);
        glColor3f(0,0,0);
	glutWireCube(comida.tamanho);
    glPopMatrix();

    colisao();
    glutSwapBuffers();
}


void tecla(unsigned char c, int a, int b)
{
    switch (c)
    {
	case 'w':
	player1.x += 0.5;
	glutPostRedisplay();
	 break;
	case 's':
	player1.x -= 0.5;
	glutPostRedisplay();
	 break;
        case 'd':
	player1.z += 0.5;
	glutPostRedisplay();
	 break;
        case 'a':
	player1.z -= 0.5;
	glutPostRedisplay();
	 break;
        case 'i':
	player2.x += 0.5;
	glutPostRedisplay();
	 break;
        case 'k':
	player2.x -= 0.5;
	glutPostRedisplay();
	 break;
	case 'l':
	player2.z += 0.5;
	glutPostRedisplay();
	 break;
	case 'j':
	player2.z -= 0.5;
	glutPostRedisplay();
	 break;
    }
    glutPostRedisplay();
}

int main(int argc, char** argv)
{
    srand(time(NULL));
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE);
    glutInitWindowSize(700, 700);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Come comi");

    glEnable(GL_DEPTH_TEST);

    glutDisplayFunc(myDisplay);
    glutReshapeFunc(myReshape);
    glutKeyboardFunc(tecla);

 
    inicializar();

    glutMainLoop();
    return 0;
}
